## 1. Application Android

L'application Android est située dans le dossier `Application android`. Pour ouvrir ce projet et le modifier, suivez les étapes ci-dessous :

### Prérequis
- Android Studio installé sur votre machine.
- Une version compatible du SDK Android (recommandé : 33 ou plus récent).

### Instructions
1. Ouvrez Android Studio.
2. Cliquez sur **File** > **Open...**.
3. Naviguez vers le dossier `Application android` situé dans ce dépôt.
4. Sélectionnez le dossier et cliquez sur **OK**.
5. Laissez Android Studio synchroniser les dépendances Gradle.
6. Une fois la synchronisation terminée, vous pouvez exécuter ou modifier l'application.

## 2. ESP32-ANDROID

Le projet ESP32-ANDROID est un projet basé sur ESP-IDF pour le microcontrôleur ESP32. Voici les étapes pour construire et flasher le projet sur un ESP32.

### Prérequis
- [ESP-IDF](https://github.com/espressif/esp-idf) installé sur votre machine.
- Python 3.6+ configuré.
- Un câble USB pour connecter l'ESP32 à votre ordinateur.

### Instructions

#### 1. Configuration de l'environnement
1. Clonez le dépôt ESP-IDF depuis son [repository officiel](https://github.com/espressif/esp-idf) :
   ```bash
   git clone --recursive https://github.com/espressif/esp-idf.git
   ```
2. Initialisez l'environnement ESP-IDF :
   ```bash
   cd esp-idf
   ./install.sh
   . ./export.sh
   ```

#### 2. Construction et flash du projet
1. Naviguez dans le dossier `ESP32-ANDROID` :
   ```bash
   cd ESP32-ANDROID
   ```
2. Configurez le projet pour sélectionner le bon port série et d'autres paramètres :
   ```bash
   idf.py menuconfig
   ```
3. Compilez le projet :
   ```bash
   idf.py build
   ```
4. Flashez le projet sur votre ESP32 :
   ```bash
   idf.py flash
   ```
5. Ouvrez le moniteur série pour voir les logs :
   ```bash
   idf.py monitor
   ```

#### 3. Raccourci pour les étapes build + flash + monitor
Vous pouvez combiner les étapes 3, 4 et 5 en une seule commande :
```bash
idf.py flash monitor
```

## 2. ESP32-DRONE
Même chose que ESP32-ANDROID mais dans le répertoire ESP32-DRONE

