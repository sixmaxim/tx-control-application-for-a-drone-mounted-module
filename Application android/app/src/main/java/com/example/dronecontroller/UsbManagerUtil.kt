package com.example.dronecontroller

import android.content.Context
import android.hardware.usb.UsbManager

object UsbManagerUtil {
    private var usbManager: UsbManager? = null

    fun initialize(context: Context) {
        if (usbManager == null) {
            usbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
        }
    }

    fun getUsbManager(): UsbManager {
        return usbManager
            ?: throw IllegalStateException("UsbManager not initialized. Call UsbManagerUtil.initialize(context) first.")
    }
}
