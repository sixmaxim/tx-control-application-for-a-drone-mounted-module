package com.example.dronecontroller

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.File

class CsvViewerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_csv_viewer)

        val textView: TextView = findViewById(R.id.csvContent)
        val filePath = intent.getStringExtra("FILE_PATH") ?: return

        val csvContent = File(filePath).readText()
        textView.text = csvContent
    }
}
