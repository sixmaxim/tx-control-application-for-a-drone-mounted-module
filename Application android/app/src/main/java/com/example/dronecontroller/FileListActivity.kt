package com.example.dronecontroller

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.File

class FileListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_list)

        // Obtenir le chemin du répertoire depuis l'intent
        val directoryPath = intent.getStringExtra("DIRECTORY_PATH")
        if (directoryPath == null) {
            Toast.makeText(this, "Directory path not provided.", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        val directory = File(directoryPath)
        if (!directory.exists() || !directory.isDirectory) {
            Toast.makeText(this, "Invalid directory path.", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        // Récupérer tous les fichiers dans le répertoire
        val files = directory.listFiles()?.toList() ?: emptyList()

        // Configurer le RecyclerView
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = FileListAdapter(files) { file ->
            // Action lors du clic sur un fichier
            Toast.makeText(this, "Clicked: ${file.name}", Toast.LENGTH_SHORT).show()
        }
    }
}
