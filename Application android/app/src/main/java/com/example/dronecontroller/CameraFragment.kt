package com.example.dronecontroller

import android.app.PendingIntent
import android.content.Intent
import android.graphics.SurfaceTexture
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.dronecontroller.databinding.FragmentUvcBinding
import com.jiangdg.ausbc.CameraClient
import com.jiangdg.ausbc.MultiCameraClient
import com.jiangdg.ausbc.base.CameraFragment
import com.jiangdg.ausbc.callback.ICameraStateCallBack
import com.jiangdg.ausbc.callback.ICaptureCallBack
import com.jiangdg.ausbc.camera.CameraUvcStrategy
import com.jiangdg.ausbc.camera.bean.CameraRequest
import com.jiangdg.ausbc.render.env.RotateType
import com.jiangdg.ausbc.widget.IAspectRatio

class CameraFragment : CameraFragment() {

      private var mViewBinding: FragmentUvcBinding? = null
//    private var isCameraOpened = false
//    private var multiCameraClient: MultiCameraClient? = null
//    private var camera: MultiCameraClient.Camera? = null
      private var mCameraClient: CameraClient? = null
      private lateinit var textureView: TextureView


    companion object {
        private const val ACTION_USB_PERMISSION = "com.example.dronecontroller.USB_PERMISSION"
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun initView() {
        super.initView()
        // Assurez-vous d'avoir défini le layout contenant texture_view
        textureView = requireView().findViewById(R.id.tv_camera_render)
        textureView.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
            override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
                surface.setDefaultBufferSize(1280, 720) // Par exemple
            }

            override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {}
            override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean = true
            override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
        }

    }

//    override fun initData() {
//
//        super.initData()
//
//        mCameraClient = getCameraClient() ?: CameraClient.newBuilder(requireContext())
//
//            .setEnableGLES(true)
//
//            .setRawImage(false)
//
//            .setCameraStrategy(CameraUvcStrategy(requireContext()))
//
//            .setCameraRequest(CameraRequest.Builder().setPreviewWidth(960).setPreviewHeight(1280).create())
//
//            .setDefaultRotateType(RotateType.ANGLE_0) // Appliquer une rotation de 90 degrés
//
//            .openDebug(true)
//
//            .build()
//
//
//    }

//    override fun getCameraClient(): CameraClient? {
//        return CameraClient.newBuilder(requireContext())
//            .setEnableGLES(true)
//            .setRawImage(false)
//            .setCameraStrategy(CameraUvcStrategy(requireContext()))
//            .setCameraRequest(
//                CameraRequest.Builder()
//                    .setFrontCamera(false)
//                    .setPreviewWidth(352) // Essayez une taille supportée
//                    .setPreviewHeight(288)
//                    .create()
//            )
//            .setDefaultRotateType(RotateType.ANGLE_0) // Ajustez si nécessaire
//            .openDebug(true)
//            .build()
//
//    }

    override fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View? {
        if (mViewBinding == null) {
            mViewBinding = FragmentUvcBinding.inflate(inflater, container, false)
        }
        return mViewBinding?.root
    }

    override fun getCameraView(): IAspectRatio? {
        return mViewBinding?.tvCameraRender
    }

    override fun getCameraViewContainer(): ViewGroup? {
        return mViewBinding?.container
    }

    override fun getGravity(): Int = Gravity.TOP
}
