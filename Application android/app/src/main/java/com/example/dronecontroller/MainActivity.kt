package com.example.dronecontroller

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.os.Environment
import android.os.PowerManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import com.jiangdg.ausbc.utils.Utils
import java.io.File
import java.io.UnsupportedEncodingException
import java.io.FileWriter
import java.io.IOException
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.view.TextureView
import androidx.core.content.FileProvider
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MainActivity : AppCompatActivity() {

    private lateinit var cameraFragment: CameraFragment
    private var serialPort: UsbSerialDevice? = null
    private var esp32Device: UsbDevice? = null
    private lateinit var cameraStatusText: TextView
    private lateinit var timestampTextView: TextView
    private lateinit var altitudeTextView: TextView
    private lateinit var temperatureTextView: TextView
    private lateinit var startMeasureButton: Button
    private lateinit var stopMeasureButton: Button
    private var mWakeLock: PowerManager.WakeLock? = null
    private lateinit var textureView: TextureView
    private var csvFileWriter: FileWriter? = null
    private var isMeasuring = false

    companion object {
        const val ACTION_USB_PERMISSION = "com.example.dronecontroller.USB_PERMISSION"
        const val REQUEST_CAMERA = 0
        const val TAG = "MainActivity"
    }

    private fun checkAndRequestPermissions() {
        val permissions = arrayOf(
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.RECORD_AUDIO

        )

        val permissionsToRequest = permissions.filter {
            ActivityCompat.checkSelfPermission(this, it) != PermissionChecker.PERMISSION_GRANTED
        }

        if (permissionsToRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toTypedArray(), REQUEST_CAMERA)
        }
    }

    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                ACTION_USB_PERMISSION -> {
                    val device = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                    if (device == null) {
                        Log.e(TAG, "UsbDevice is null in ACTION_USB_PERMISSION")
                        Toast.makeText(context, "Device is null. Please reconnect the USB device.", Toast.LENGTH_SHORT).show()
                        return
                    }

                    val permissionGranted = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                    if (permissionGranted) {
                        connectToUsbDevice(device)
                    } else {
                        Toast.makeText(context, "Permission denied for device ${device.deviceName}", Toast.LENGTH_SHORT).show()
                    }
                }
                UsbManager.ACTION_USB_DEVICE_ATTACHED -> {
                    val device = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                    if (device == null) {
                        Log.e(TAG, "UsbDevice is null in ACTION_USB_DEVICE_ATTACHED")
                        return
                    }

                    if (isEsp32Device(device)) {
                        esp32Device = device
                        requestUsbPermission(device)
                    }
                }
                UsbManager.ACTION_USB_DEVICE_DETACHED -> {
                    val device = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                    if (device == null) {
                        Log.e(TAG, "UsbDevice is null in ACTION_USB_DEVICE_DETACHED")
                        return
                    }

                    if (device == esp32Device) {
                        disconnectUsbDevice()
                        esp32Device = null
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkAndRequestPermissions()

        // Initialiser UsbManager
        UsbManagerUtil.initialize(this)

        // Enregistrer le BroadcastReceiver
        val filter = IntentFilter()
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        filter.addAction(ACTION_USB_PERMISSION)
        registerReceiver(usbReceiver, filter)

        // Initialiser les composants UI
        cameraStatusText = findViewById(R.id.camera_status_text)
        timestampTextView = findViewById(R.id.timestampTextView)
        altitudeTextView = findViewById(R.id.altitudeTextView)
        temperatureTextView = findViewById(R.id.temperatureTextView)
        startMeasureButton = findViewById(R.id.startMeasureButton)
        stopMeasureButton = findViewById(R.id.stopMeasureButton)

        // Initialiser les composants UI
        val capturePhotoDataButton: Button = findViewById(R.id.capturePhotoDataButton)
        // Configurer le bouton pour capturer la photo
        capturePhotoDataButton.setOnClickListener {
            capturePhotoWithOverlay(timestampTextView.text.toString(),altitudeTextView.text.toString(),temperatureTextView.text.toString())
        }

        val openImagesDirectoryButton: Button = findViewById(R.id.openImagesDirectoryButton)
        val openCsvDirectoryButton: Button = findViewById(R.id.openCsvDirectoryButton)

// Bouton pour ouvrir le répertoire des images
        openImagesDirectoryButton.setOnClickListener {
            val imagesDir = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DroneImages")
            if (!imagesDir.exists()) {
                imagesDir.mkdirs()
            }
            openFileManager(imagesDir.absolutePath)
        }

// Bouton pour ouvrir le répertoire des CSV
        openCsvDirectoryButton.setOnClickListener {
            val csvDir = File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "DroneData")
            if (!csvDir.exists()) {
                csvDir.mkdirs()
            }
            openFileManager(csvDir.absolutePath)
        }

        // Charger CameraFragment
        cameraFragment = CameraFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, cameraFragment)
            .commit()

        // Vérifier les appareils USB connectés au démarrage
        val usbManager = UsbManagerUtil.getUsbManager()
        for (device in usbManager.deviceList.values) {
            if (isEsp32Device(device)) {
                esp32Device = device
                if (!usbManager.hasPermission(device)) {
                    requestUsbPermission(device)
                } else {
                    connectToUsbDevice(device)
                }
            }
        }

        startMeasureButton.setOnClickListener {
            if (sendCommandToUsbDevice("startMesure\n")) {
                startDataAcquisition() // Commence l'acquisition continue
            } else {
                Toast.makeText(this, "Failed to start measure. Command not sent.", Toast.LENGTH_SHORT).show()
            }
        }

        stopMeasureButton.setOnClickListener {
            if (sendCommandToUsbDevice("stopMesure\n")) {
                stopDataAcquisition() // Arrête l'acquisition continue
            } else {
                Toast.makeText(this, "Failed to stop measure. Command not sent.", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun openFileManager(path: String) {
        val intent = Intent(this, FileListActivity::class.java).apply {
            putExtra("DIRECTORY_PATH", path)
        }
        startActivity(intent)
    }

    // Méthode pour démarrer l'acquisition
    private fun startDataAcquisition() {
        try {
            // Créer un dossier pour stocker les fichiers CSV
            val dir = File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "DroneData")
            if (!dir.exists()) {
                dir.mkdirs()
            }

            // Nom du fichier basé sur l'heure actuelle
            val csvFile = File(dir, "capture_${System.currentTimeMillis()}.csv")
            csvFileWriter = FileWriter(csvFile, true) // Ouvrir en mode "append"

            // Écrire les en-têtes dans le fichier
            csvFileWriter?.append("Timestamp,Altitude,Temperature\n")
            csvFileWriter?.flush()

            isMeasuring = true
            Toast.makeText(this, "Acquisition started: ${csvFile.absolutePath}", Toast.LENGTH_LONG).show()
        } catch (e: IOException) {
            Toast.makeText(this, "Failed to start acquisition: ${e.message}", Toast.LENGTH_SHORT).show()
        }
    }

    // Méthode pour arrêter l'acquisition
    private fun stopDataAcquisition() {
        try {
            csvFileWriter?.close()
            isMeasuring = false
            Toast.makeText(this, "Acquisition stopped.", Toast.LENGTH_LONG).show()
        } catch (e: IOException) {
            Toast.makeText(this, "Failed to stop acquisition: ${e.message}", Toast.LENGTH_SHORT).show()
        }
    }

    // Méthode pour enregistrer les données pendant l'acquisition
    private fun appendDataToCsv(timestamp: String, altitude: String, temperature: String) {
        if (isMeasuring && csvFileWriter != null) {
            try {
                csvFileWriter?.append("$timestamp,$altitude,$temperature\n")
                csvFileWriter?.flush()
            } catch (e: IOException) {
                Log.e(TAG, "Error writing to CSV file: ${e.message}", e)
            }
        }
    }

    private fun capturePhotoWithOverlay(timestamp: String, altitude: String, temperature: String) {
        val textureView: TextureView = findViewById(R.id.tv_camera_render)

        if (textureView.isAvailable) {
            val bitmap = textureView.bitmap
            val overlayBitmap = bitmap?.let { Bitmap.createBitmap(it.width, it.height, it.config) }
            val canvas = overlayBitmap?.let { Canvas(it) }

            // Dessiner l'image capturée
            if (bitmap != null) {
                canvas?.drawBitmap(bitmap, 0f, 0f, null)
            }

            // Obtenir l'heure actuelle du téléphone
            val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())

            // Préparer la peinture pour le texte
            val textPaint = Paint().apply {
                color = Color.WHITE
                textSize = 50f
                isAntiAlias = true
            }

            // Préparer la peinture pour le fond (rectangle opaque)
            val backgroundPaint = Paint().apply {
                color = Color.BLACK
                alpha = 150 // Opacité à 60 % (valeur entre 0 et 255)
            }

            // Définir les positions du texte
            val textLines = listOf(currentTime, timestamp, altitude, temperature)
            val textPadding = 10f
            val textHeight = textPaint.textSize + textPadding
            var yPosition = 50f

            for (line in textLines) {
                val textWidth = textPaint.measureText(line)
                val backgroundRect = RectF(
                    20f - textPadding, // Left
                    yPosition - textHeight + textPadding, // Top
                    20f + textWidth + textPadding, // Right
                    yPosition + textPadding // Bottom
                )

                // Dessiner le rectangle opaque
                canvas?.drawRoundRect(backgroundRect, 8f, 8f, backgroundPaint)

                // Dessiner le texte
                canvas?.drawText(line, 20f, yPosition, textPaint)

                yPosition += textHeight
            }

            try {
                val dir = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DroneImages")
                if (!dir.exists()) {
                    dir.mkdirs()
                }

                val file = File(dir, "capture_${System.currentTimeMillis()}.png")
                FileOutputStream(file).use { outputStream ->
                    overlayBitmap?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                    outputStream.flush()
                }

                Toast.makeText(this, "Photo with overlay saved to ${file.absolutePath}", Toast.LENGTH_LONG).show()
            } catch (e: IOException) {
                Toast.makeText(this, "Failed to save photo: ${e.message}", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "TextureView is not ready", Toast.LENGTH_SHORT).show()
        }
    }



    private fun isEsp32Device(device: UsbDevice): Boolean {
        // Modifier selon le VendorID/ProductID de l'ESP32
        return device.vendorId == 0x1a86 // Exemple : remplacer par le bon VendorID
    }

    @SuppressLint("MutableImplicitPendingIntent", "UnspecifiedImmutableFlag")
    private fun requestUsbPermission(device: UsbDevice) {
        val permissionIntent = PendingIntent.getBroadcast(this, 0, Intent(ACTION_USB_PERMISSION), 0)
        UsbManagerUtil.getUsbManager().requestPermission(device, permissionIntent)
    }

    private fun connectToUsbDevice(device: UsbDevice) {
        val connection = UsbManagerUtil.getUsbManager().openDevice(device)
        serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection)
        if (serialPort != null) {
            serialPort?.apply {
                open()
                setBaudRate(115200)
                setDataBits(UsbSerialInterface.DATA_BITS_8)
                setStopBits(UsbSerialInterface.STOP_BITS_1)
                setParity(UsbSerialInterface.PARITY_NONE)
                setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF)

                read { data ->
                    try {
                        val receivedData = String(data, Charsets.UTF_8)
                        runOnUiThread { processReceivedData(receivedData) }
                    } catch (e: UnsupportedEncodingException) {
                        Log.e(TAG, "Error decoding data", e)
                    }
                }
            }
            Toast.makeText(this, "ESP32 connected: ${device.deviceName}", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Failed to connect to ESP32.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun disconnectUsbDevice() {
        serialPort?.close()
        serialPort = null
        Toast.makeText(this, "ESP32 disconnected.", Toast.LENGTH_SHORT).show()
    }

    private fun sendCommandToUsbDevice(command: String): Boolean {
        return if (serialPort != null) {
            try {
                serialPort!!.write(command.toByteArray())
                true // Commande envoyée avec succès
            } catch (e: Exception) {
                e.printStackTrace()
                false // Échec lors de l'envoi de la commande
            }
        } else {
            Toast.makeText(this, "No device connected", Toast.LENGTH_SHORT).show()
            false // Aucun appareil connecté
        }
    }

    private fun processReceivedData(data: String) {
        val parts = data.split(";")
        var timestamp = ""
        var altitude = ""
        var temperature = ""

        for (part in parts) {
            when {
                part.startsWith("timestamp=") -> {
                    timestamp = part.removePrefix("timestamp=")
                    timestampTextView.text = "Timestamp: $timestamp"
                }
                part.startsWith("alt=") -> {
                    altitude = part.removePrefix("alt=")
                    altitudeTextView.text = "Altitude: $altitude"
                }
                part.startsWith("temp=") -> {
                    temperature = part.removePrefix("temp=")
                    temperatureTextView.text = "Temperature: $temperature"
                }
            }
        }

        // Ajouter les données au fichier CSV si l'acquisition est en cours
        if (isMeasuring) {
            appendDataToCsv(timestamp, altitude, temperature)
        }
    }

    override fun onStart() {
        super.onStart()
        mWakeLock = Utils.wakeLock(this)
    }

    override fun onStop() {
        super.onStop()
        mWakeLock?.let { Utils.wakeUnLock(it) }
    }

    override fun onDestroy() {
        super.onDestroy()
        sendCommandToUsbDevice("stopMesure\n")
        unregisterReceiver(usbReceiver)
        disconnectUsbDevice()
    }
}
