package com.example.dronecontroller

import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class ImageViewerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)

        val imageView: ImageView = findViewById(R.id.imageView)
        val filePath = intent.getStringExtra("FILE_PATH") ?: return

        val bitmap = BitmapFactory.decodeFile(filePath)
        imageView.setImageBitmap(bitmap)
    }
}