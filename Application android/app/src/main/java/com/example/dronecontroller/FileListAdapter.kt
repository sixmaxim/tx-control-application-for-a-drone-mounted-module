package com.example.dronecontroller

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import com.felhr.usbserial.BuildConfig
import java.io.File

class FileListAdapter(
    private val files: List<File>,
    private val onItemClick: (File) -> Unit
) : RecyclerView.Adapter<FileListAdapter.FileViewHolder>() {

    class FileViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val fileName: TextView = view.findViewById(R.id.fileName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_file, parent, false)
        return FileViewHolder(view)
    }

    override fun onBindViewHolder(holder: FileViewHolder, position: Int) {
        val file = files[position]
        holder.fileName.text = file.name
        holder.itemView.setOnClickListener {
            if (file.isFile) {
                val mimeType = when {
                    file.name.endsWith(".jpg", ignoreCase = true) ||
                            file.name.endsWith(".jpeg", ignoreCase = true) ||
                            file.name.endsWith(".png", ignoreCase = true) -> "image/*"
                    file.name.endsWith(".csv", ignoreCase = true) -> "text/csv"
                    else -> "*/*"
                }
                when (mimeType) {
                    "image/*" -> {
                        val intent = Intent(it.context, ImageViewerActivity::class.java).apply {
                            putExtra("FILE_PATH", file.absolutePath)
                        }
                        it.context.startActivity(intent)
                    }
                    "text/csv" -> {
                        val intent = Intent(it.context, CsvViewerActivity::class.java).apply {
                            putExtra("FILE_PATH", file.absolutePath)
                        }
                        it.context.startActivity(intent)
                    }
                    else -> {
                        val uri = FileProvider.getUriForFile(it.context, "${BuildConfig.APPLICATION_ID}.provider", file)
                        val intent = Intent(Intent.ACTION_VIEW).apply {
                            setDataAndType(uri, mimeType)
                            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        }
                        it.context.startActivity(intent)
                    }
                }
            } else {
                Toast.makeText(it.context, "This is a directory.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun getItemCount(): Int = files.size
}