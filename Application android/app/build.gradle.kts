plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
}

android {
    namespace = "com.example.dronecontroller"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.dronecontroller"
        minSdk = 29
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    // Configuring JVM versions to use the same for both Java and Kotlin
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    // Optional: Use a JVM toolchain to ensure the correct version of Java is used
    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(17))
        }
    }

    // Enable Jetpack Compose
    buildFeatures {
        compose = true
        viewBinding = true
        buildConfig = true
    }

    // Specify the Kotlin compiler extension version for Jetpack Compose
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.2" // Make sure this is compatible with Kotlin 1.9.0
    }
}

dependencies {

    implementation("com.github.jiangdongguo.AndroidUSBCamera:libausbc:3.2.7") {
        exclude(group = "com.gyf.immersionbar", module = "immersionbar")
        exclude(group = "com.zlc.glide", module = "webpdecoder")
    }

    implementation("com.github.felHR85:UsbSerial:6.1.0")

    implementation("com.github.felHR85:UsbSerial:6.1.0")
    implementation("org.videolan.android:libvlc-all:3.3.0")
    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.5.0")
    implementation("com.google.android.material:material:1.6.0")

    // Jetpack Compose dependencies
    implementation("androidx.compose.ui:ui:1.5.0")
    implementation("androidx.compose.material:material:1.5.0")

    // Material Design 3 (Material You) for Jetpack Compose
    implementation("androidx.compose.material3:material3:1.1.0")
    implementation(libs.espresso.core)
    implementation(libs.espresso.core)
    implementation(libs.androidx.camera.core)

    // Kapt (Kotlin Annotation Processing)
    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.13.0")

    // Debugging and testing tools for Compose
    debugImplementation("androidx.compose.ui:ui-tooling:1.5.0")
    debugImplementation("androidx.compose.ui:ui-tooling-preview:1.5.0")

    // Testing dependencies
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.5.0")
    debugImplementation("androidx.compose.ui:ui-test-manifest:1.5.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.recyclerview:recyclerview:1.3.0")
    implementation("androidx.activity:activity-ktx:1.8.0")
}
