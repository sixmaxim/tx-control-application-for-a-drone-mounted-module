plugins {
    id("com.android.application") version "8.6.0" apply false
    id("com.android.library") version "8.6.0" apply false
    id("org.jetbrains.kotlin.android") version "1.9.0" apply false // Update to 1.9.0
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

