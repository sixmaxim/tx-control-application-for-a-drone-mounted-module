#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "driver/gpio.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_system.h"
#include "esp_timer.h" // Include this header for esp_timer_get_time()
#include "driver/i2c_master.h"
#include "driver/i2c_types.h"
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define SENSOR_ADDR 0x60
#define GPIO_SDA GPIO_NUM_21
#define GPIO_SCL GPIO_NUM_22

typedef struct
{
    float pressure;
    float altitude;
    float temperature;
    int status; // 0 = success, 1 = error
} sensor_data_t;

esp_err_t sensor_write_register(i2c_master_dev_handle_t dev_handle, uint8_t reg_addr, uint8_t data)
{
    uint8_t buffer[2] = {reg_addr, data};
    return i2c_master_transmit(dev_handle, buffer, sizeof(buffer), -1);
}

esp_err_t sensor_read_register(i2c_master_dev_handle_t dev_handle, uint8_t reg_addr, uint8_t *data_rd, size_t length)
{
    return i2c_master_transmit_receive(dev_handle, &reg_addr, sizeof(reg_addr), data_rd, length, -1);
}

float sensor_read_altitude(i2c_master_dev_handle_t dev_handle)
{
    uint8_t msb, csb, lsb;
    uint8_t reg_msb = 0x01, reg_csb = 0x02, reg_lsb = 0x03;
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_msb, &msb, sizeof(msb)));
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_csb, &csb, sizeof(csb)));
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_lsb, &lsb, sizeof(lsb)));

    int32_t altitude_raw = ((int32_t)msb << 12) | ((int32_t)csb << 4) | ((int32_t)(lsb >> 4));

    // Check signed bit (20th)
    if (altitude_raw & (1 << 19))
    {
        altitude_raw |= 0xFFF00000; // sign extension
    }

    return altitude_raw / 16.0f;
}
float sensor_read_temperature(i2c_master_dev_handle_t dev_handle)
{
    uint8_t msb, lsb;
    uint8_t reg_msb = 0x04, reg_lsb = 0x05;
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_msb, &msb, sizeof(msb)));
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_lsb, &lsb, sizeof(lsb)));

    int16_t temp_raw = ((int16_t)msb << 4) | ((int16_t)(lsb >> 4));

    // Check signed bit (20th)
    if (temp_raw & (1 << 11))
    {
        temp_raw |= 0xF000; // sign extension
    }

    return temp_raw / 16.0f;
}


void sensor_set_reference_pressure(i2c_master_dev_handle_t dev_handle)
{
    // Sets Compiègne Pression as reference
    uint8_t reg_bar_in_msb = 0x14;
    uint8_t reg_bar_in_lsb = 0x15;

    uint16_t sealevel_pressure = 102700  / 2; // Pression in Compiègne - Need to divide by 2 as per sensor documentation
    uint8_t msb = (sealevel_pressure >> 8) & 0xFF; // Most significant byte
    uint8_t lsb = sealevel_pressure & 0xFF;        // Least significant byte

    ESP_ERROR_CHECK(sensor_write_register(dev_handle, reg_bar_in_msb, msb));
    ESP_ERROR_CHECK(sensor_write_register(dev_handle, reg_bar_in_lsb, lsb));
}

int sensor_check_who_am_i(i2c_master_dev_handle_t dev_handle){
    uint8_t who_am_i;
    uint8_t reg_who_am_i = 0x0c;
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_who_am_i, &who_am_i, sizeof(who_am_i)));
    
    if (who_am_i == 0xC4)
    {
        return 1;
    }
    return 0;
}

int sensor_get_mode(i2c_master_dev_handle_t dev_handle){
    uint8_t mode;
    uint8_t reg_mode = 0x26;
    ESP_ERROR_CHECK(sensor_read_register(dev_handle, reg_mode, &mode, sizeof(mode)));
    return mode;
}

i2c_master_dev_handle_t init_sensor()
{   
    i2c_master_bus_config_t i2c_mst_config = {
        .clk_source = I2C_CLK_SRC_DEFAULT,
        .i2c_port = I2C_NUM_0,
        .scl_io_num = GPIO_SCL,
        .sda_io_num = GPIO_SDA,
        .glitch_ignore_cnt = 7,
        .flags.enable_internal_pullup = true,
    };

    i2c_master_bus_handle_t bus_handle;
    ESP_ERROR_CHECK(i2c_new_master_bus(&i2c_mst_config, &bus_handle));

    i2c_device_config_t dev_cfg = {
        .dev_addr_length = I2C_ADDR_BIT_LEN_7,
        .device_address = SENSOR_ADDR,
        .scl_speed_hz = 100000,
    };

    i2c_master_dev_handle_t dev_handle;
    ESP_ERROR_CHECK(i2c_master_bus_add_device(bus_handle, &dev_cfg, &dev_handle));

    ESP_ERROR_CHECK(sensor_write_register(dev_handle, 0x26, 0xB8)); // Set to Altimeter with an OSR = 128
    ESP_ERROR_CHECK(sensor_write_register(dev_handle, 0x13, 0x07)); // Enable Data Flags in PT_DATA_CFG
    ESP_ERROR_CHECK(sensor_write_register(dev_handle, 0x26, 0xB9)); // 0xB9 : Altimeter mode - 0x39 : Barometer mode

    if(sensor_check_who_am_i(dev_handle) != 1){
        perror("Erreur lecture capteur\n");
    }
    sensor_set_reference_pressure(dev_handle);

    return dev_handle;
}

sensor_data_t measure(i2c_master_dev_handle_t dev_handle)
{
    sensor_data_t result = {0, 0, 0, 1};
    uint8_t status = 0;
    uint8_t reg_status = 0x00;
    int timeout = 5;

    while (!(status & 0x08) && timeout>0)
    {
        esp_err_t ret = sensor_read_register(dev_handle, reg_status, &status, sizeof(status));
        if (ret != ESP_OK)
        {
            result.status = 1;
        }else{

            float altitude = sensor_read_altitude(dev_handle);
            float temperature = sensor_read_temperature(dev_handle);

            result.altitude = altitude;
            result.temperature = temperature;
        }
        timeout--;
    }

    return result;
}