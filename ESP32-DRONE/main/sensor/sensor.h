#ifndef SENSOR_H
#define SENSOR_H

#include <stdint.h>
#include <stddef.h>
#include "esp_err.h"
#include "driver/i2c_types.h"

// Constants
#define SENSOR_ADDR 0x60
#define GPIO_SDA GPIO_NUM_21
#define GPIO_SCL GPIO_NUM_22

// Data structure for sensor readings
typedef struct {
    float pressure;     // Pressure in Pascals
    float altitude;     // Altitude in meters
    float temperature;  // Temperature in Celsius
    int status;         // 0 = success, 1 = error
} sensor_data_t;

// Function prototypes
i2c_master_dev_handle_t init_sensor(void);

esp_err_t sensor_write_register(i2c_master_dev_handle_t dev_handle, uint8_t reg_addr, uint8_t data);
esp_err_t sensor_read_register(i2c_master_dev_handle_t dev_handle, uint8_t reg_addr, uint8_t *data_rd, size_t length);

float sensor_read_altitude(i2c_master_dev_handle_t dev_handle);
float sensor_read_temperature(i2c_master_dev_handle_t dev_handle);
void sensor_set_reference_pressure(i2c_master_dev_handle_t dev_handle);
sensor_data_t measure(i2c_master_dev_handle_t dev_handle);


#endif // SENSOR_H
