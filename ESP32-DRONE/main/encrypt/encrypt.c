#include "mbedtls/aes.h"
#include "mbedtls/base64.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sensor/sensor.h"
#include "esp_log.h"
#include <ctype.h>

#define AES_BLOCK_SIZE 16

void pad_string(const char *input, unsigned char **padded, size_t *padded_len)
{
    size_t input_len = strlen(input);
    size_t padding_needed = AES_BLOCK_SIZE - (input_len % AES_BLOCK_SIZE);
    *padded_len = input_len + padding_needed;

    *padded = (unsigned char *)malloc(*padded_len);

    memcpy(*padded, input, input_len);

    memset(*padded + input_len, padding_needed, padding_needed);
}

void unpad_string(unsigned char *padded, size_t *unpadded_len)
{
    size_t padding_len = padded[*unpadded_len - 1];
    *unpadded_len -= padding_len;
}

unsigned char *aes_encrypt(const char *input, const unsigned char *key, size_t *output_len)
{
    unsigned char *padded_input;
    size_t padded_len;
    pad_string(input, &padded_input, &padded_len);

    unsigned char *output = (unsigned char *)malloc(padded_len);

    mbedtls_aes_context aes;
    mbedtls_aes_init(&aes);
    mbedtls_aes_setkey_enc(&aes, key, 128);

    // Encrypt in 16-byte blocks
    for (size_t i = 0; i < padded_len; i += AES_BLOCK_SIZE)
    {
        mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_ENCRYPT, padded_input + i, output + i);
    }

    *output_len = padded_len;
    mbedtls_aes_free(&aes);
    free(padded_input);

    return output;
}

unsigned char *aes_decrypt(const unsigned char *input, size_t input_len, const unsigned char *key, size_t *output_len)
{
    unsigned char *output = (unsigned char *)malloc(input_len);

    mbedtls_aes_context aes;
    mbedtls_aes_init(&aes);
    mbedtls_aes_setkey_dec(&aes, key, 128);

    // Decrypt in 16-byte blocks
    for (size_t i = 0; i < input_len; i += AES_BLOCK_SIZE)
    {
        mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_DECRYPT, input + i, output + i);
    }

    mbedtls_aes_free(&aes);

    // Remove padding
    *output_len = input_len;
    unpad_string(output, output_len);

    return output;
}

unsigned char *base64_encode(const unsigned char *input, size_t input_len, size_t *output_len)
{
    size_t b64_len;
    unsigned char *b64_output;

    // Calculate the base64 encoded length
    mbedtls_base64_encode(NULL, 0, &b64_len, input, input_len);

    // Allocate memory for base64 encoded output
    b64_output = (unsigned char *)malloc(b64_len);

    // Perform base64 encoding
    mbedtls_base64_encode(b64_output, b64_len, output_len, input, input_len);

    return b64_output;
}

unsigned char *base64_decode(const unsigned char *input, size_t input_len, size_t *output_len)
{
    size_t b64_decoded_len;
    unsigned char *decoded_output;

    mbedtls_base64_decode(NULL, 0, &b64_decoded_len, input, input_len);

    // Allocate memory for decoded output
    decoded_output = (unsigned char *)malloc(b64_decoded_len + 1);

    // Perform base64 decoding
    mbedtls_base64_decode(decoded_output, b64_decoded_len, output_len, input, input_len);
    decoded_output[*output_len] = '\0'; 

    return decoded_output;
}

int is_valid_base64_char(char c)
{
    return isalnum(c) || c == '+' || c == '/' || c == '=';
}

char *encode_message(char *message)
{
    if (message == NULL)
    {
        return NULL; 
    }

    const unsigned char key[16] = "NZqHjafILr7HPVx0";
    
    size_t encrypted_len;
    unsigned char *encrypted = aes_encrypt((char *)message, key, &encrypted_len);

    if (encrypted == NULL)
    {
        return NULL; // Encryption failed
    }


    size_t base64_len;
    unsigned char *base64_message = base64_encode(encrypted, encrypted_len, &base64_len);

    if (base64_message == NULL)
    {
        free(encrypted);
        return NULL; // Base64 encoding failed
    }

    free(encrypted);

    return (char *) base64_message;
}

char *decode_message(char *message)
{

    // ESP_LOGI("DEBUG", "MSG = %s -> %d", message,strlen(message) );
    if (message == NULL)
    {
        return NULL; 
    }

    // Validate Base64 input
    size_t msg_len = strlen(message);
    for (size_t i = 0; i < msg_len; i++)
    {
        if (!is_valid_base64_char(message[i]))
        {
            ESP_LOGE("DEBUG", "Invalid Base64 character found: %c", message[i]);
            return NULL; // Return NULL if invalid character is found
        }
    }

    const unsigned char key[16] = "NZqHjafILr7HPVx0";

    size_t base64_len;
    unsigned char *encrypted = base64_decode((unsigned char *)message, strlen(message), &base64_len);
    if (encrypted == NULL)
    {
        free(encrypted);
        return NULL;
    }
    

    size_t decrypted_len;
    unsigned char *decrypted = aes_decrypt(encrypted, base64_len, key, &decrypted_len);
    decrypted[decrypted_len] = '\0';

    if (decrypted == NULL)
    {
        free(encrypted);
        return NULL;
    }

    free(encrypted);

    return (char *) decrypted;
}