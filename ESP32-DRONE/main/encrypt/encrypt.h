#ifndef ENCRYPT_H
#define ENCRYPT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mbedtls/aes.h"
#include "mbedtls/base64.h"

#define AES_BLOCK_SIZE 16

/**
 * @brief Pads the input string to a length that is a multiple of AES_BLOCK_SIZE.
 * @param input The input string.
 * @param padded A pointer to the padded output string.
 * @param padded_len A pointer to the length of the padded string.
 */
void pad_string(const char *input, unsigned char **padded, size_t *padded_len);

/**
 * @brief Removes padding from a padded string.
 * @param padded The padded string.
 * @param unpadded_len A pointer to the length of the string after removing padding.
 */
void unpad_string(unsigned char *padded, size_t *unpadded_len);

/**
 * @brief Encrypts the input string using AES in ECB mode.
 * @param input The input string.
 * @param key The encryption key (128 bits).
 * @param output_len A pointer to the length of the encrypted output.
 * @return The encrypted output (allocated memory to be freed by the caller).
 */
unsigned char *aes_encrypt(const char *input, const unsigned char *key, size_t *output_len);

/**
 * @brief Decrypts the input data using AES in ECB mode.
 * @param input The encrypted input data.
 * @param input_len The length of the encrypted input data.
 * @param key The decryption key (128 bits).
 * @param output_len A pointer to the length of the decrypted output.
 * @return The decrypted output (allocated memory to be freed by the caller).
 */
unsigned char *aes_decrypt(const unsigned char *input, size_t input_len, const unsigned char *key, size_t *output_len);

/**
 * @brief Encodes the input data to Base64.
 * @param input The input data.
 * @param input_len The length of the input data.
 * @param output_len A pointer to the length of the Base64 encoded output.
 * @return The Base64 encoded output (allocated memory to be freed by the caller).
 */
unsigned char *base64_encode(const unsigned char *input, size_t input_len, size_t *output_len);

/**
 * @brief Decodes the Base64 encoded input data.
 * @param input The Base64 encoded input data.
 * @param input_len The length of the Base64 encoded input data.
 * @param output_len A pointer to the length of the decoded output.
 * @return The decoded output (allocated memory to be freed by the caller).
 */
unsigned char *base64_decode(const unsigned char *input, size_t input_len, size_t *output_len);

char *encode_message(char *message);

char *decode_message(char *message);

#endif // ENCRYPT_H
