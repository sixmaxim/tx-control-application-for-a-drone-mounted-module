#include <stdio.h>
#include <string.h>
#include "driver/uart.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"

#include "sensor/sensor.h"
#include "encrypt/encrypt.h"

#define UART_NUM UART_NUM_2
#define BUF_SIZE (2048) // Taille du tampon UART

#define LORA_M0_PIN GPIO_NUM_4
#define LORA_M1_PIN GPIO_NUM_2

static QueueHandle_t uart_queue; // File d'attente pour les événements UART
static char data[BUF_SIZE];

void lora_init() {
    // Réinitialisation et configuration des GPIO pour le mode M0 et M1
    gpio_reset_pin(LORA_M0_PIN);
    gpio_reset_pin(LORA_M1_PIN);
    gpio_set_direction(LORA_M0_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LORA_M1_PIN, GPIO_MODE_OUTPUT);

    // Configuration UART
    const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(UART_NUM, &uart_config);
    uart_set_pin(UART_NUM, GPIO_NUM_17, GPIO_NUM_16, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM, BUF_SIZE, BUF_SIZE, 10, &uart_queue, 0);

    // Passer en mode configuration (M0=1, M1=1)
    gpio_set_level(LORA_M0_PIN, 1);
    gpio_set_level(LORA_M1_PIN, 1);
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente de 100ms pour activer le mode configuration

    // Configuration du module pour longue portée
    uint8_t config_command[] = {
        0xC0, // Commande pour régler les paramètres
        0x00, // Adresse haute (par défaut : 0x00)
        0x00, // Adresse basse (par défaut : 0x00)
        0x18, // Vitesse de l'air : 2.4 kbps (optimisé pour la portée)
        0xC4, // Puissance maximale (20 dBm) et mode fixe
        0x44, // 125 kHz de bande passante, SF12, CR 4/5
        0x00  // Pas de canal personnalisé
    };

    // Envoyer la commande au module via UART
    uart_write_bytes(UART_NUM, (const char *)config_command, sizeof(config_command));
    
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente pour l'application de la configuration

    // Repasser en mode normal (M0=0, M1=0)
    gpio_set_level(LORA_M0_PIN, 0);
    gpio_set_level(LORA_M1_PIN, 0);
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente pour activer le mode normal
}

// Récupérer les données reçues par LoRa
int getLora() {
    uart_event_t event;
	if (xQueueReceive(uart_queue, (void *)&event, 0)) {
        switch (event.type){ 
            case UART_DATA: 
                // Lire les données disponibles
                int len = uart_read_bytes(UART_NUM, data, event.size, 100 / portTICK_PERIOD_MS);
            	if (len > 0) {
                	data[len] = '\0';
                    char *decrypted_data = decode_message(data);
                    if (decrypted_data == NULL) {
                        ESP_LOGE("UART", "Failed to decode the received message");
                        return -1;
                    }

                    strcpy(data, decrypted_data);
                    free(decrypted_data);
                	return len;
            	}
				break;
            case UART_FIFO_OVF:
                ESP_LOGW("LoRa", "Overflow du FIFO UART !");
                uart_flush_input(UART_NUM);
                xQueueReset(uart_queue);
                return 0;
                break;
            case UART_BUFFER_FULL:
                ESP_LOGW("LoRa", "Tampon UART plein !");
                uart_flush_input(UART_NUM);
                xQueueReset(uart_queue);
                return 0;
                break;
            default:
                ESP_LOGW("LoRa", "Événement UART non géré : %d", event.type);
                return 0;
                break;
        }
    }else{
		vTaskDelay(pdMS_TO_TICKS(10));
	}
    return 0;
}

// Envoyer un message via LoRa
void envoieLora(const char *msg) {
    ESP_LOGI("DEBUG", "Sending message : %s", msg);

    char *encrypted = encode_message(msg);
    if (encrypted == NULL) {
        ESP_LOGE("DEBUG", "Failed to encode the message");
        return;
    }
    uart_write_bytes(UART_NUM, encrypted, strlen(encrypted));
    free(encrypted);
}

// Convertir sensor_data_t en chaîne de caractères
void sensor_data_to_string(sensor_data_t data, char *buffer, size_t buffer_size) {
    snprintf(buffer, buffer_size, "alt=%.2f;temp=%.2f",
             data.altitude, data.temperature);
}

void app_main() {
    // Initialisation capteur
    i2c_master_dev_handle_t sensor_handle = init_sensor();

    // Initialisation de LoRa
    lora_init();
    bool start = false;


    while (1) {
        // Vérifier les messages reçus
        if (getLora() != 0) {
            if (strcmp((char *)data, "Give") == 0) {
                start = true;
            } else if (strcmp((char *)data, "Stop") == 0) {
                start = false;
            }
        }

        if (start) {
            sensor_data_t sensor_data = measure(sensor_handle);
            char *result = (char*) malloc(sizeof(char) * BUF_SIZE);
            // Convertir les données capteur en chaîne
            sprintf(result, "alt=%.2f;temp=%.2f;", sensor_data.altitude, sensor_data.temperature);

            // Envoyer la chaîne via LoRa
            envoieLora(result);
            
            free(result);

            vTaskDelay(1000 / portTICK_PERIOD_MS); // Pause pour ne pas surcharger
        }
    }
}