#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_random.h" 
#include "driver/uart.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "esp_timer.h"

#include "encrypt/encrypt.h"

// Buffer pour les données UART
#define UART_BUF_SIZE 1024

#define UART_NUM UART_NUM_2

#define LORA_M0_PIN GPIO_NUM_4
#define LORA_M1_PIN GPIO_NUM_2

static QueueHandle_t uart_queue; // File d'attente pour les événements UART
static char data[UART_BUF_SIZE];


void lora_init() {
    // Réinitialisation et configuration des GPIO pour le mode M0 et M1
    gpio_reset_pin(LORA_M0_PIN);
    gpio_reset_pin(LORA_M1_PIN);
    gpio_set_direction(LORA_M0_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LORA_M1_PIN, GPIO_MODE_OUTPUT);

    // Configuration UART
    const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(UART_NUM, &uart_config);
    uart_set_pin(UART_NUM, GPIO_NUM_17, GPIO_NUM_16, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM, UART_BUF_SIZE, UART_BUF_SIZE, 10, &uart_queue, 0);

    // Passer en mode configuration (M0=1, M1=1)
    gpio_set_level(LORA_M0_PIN, 1);
    gpio_set_level(LORA_M1_PIN, 1);
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente de 100ms pour activer le mode configuration

    // Configuration du module pour longue portée
    uint8_t config_command[] = {
        0xC0, // Commande pour régler les paramètres
        0x00, // Adresse haute (par défaut : 0x00)
        0x00, // Adresse basse (par défaut : 0x00)
        0x18, // Vitesse de l'air : 2.4 kbps (optimisé pour la portée)
        0xC4, // Puissance maximale (20 dBm) et mode fixe
        0x44, // 125 kHz de bande passante, SF12, CR 4/5
        0x00  // Pas de canal personnalisé
    };

    // Envoyer la commande au module via UART
    uart_write_bytes(UART_NUM, (const char *)config_command, sizeof(config_command));
    
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente pour l'application de la configuration

    // Repasser en mode normal (M0=0, M1=0)
    gpio_set_level(LORA_M0_PIN, 0);
    gpio_set_level(LORA_M1_PIN, 0);
    vTaskDelay(100 / portTICK_PERIOD_MS); // Attente pour activer le mode normal
}


// Récupérer les données reçues par LoRa
int getLora() {
    uart_event_t event;

    // Vérifier les événements UART de manière non bloquante
    if (xQueueReceive(uart_queue, (void *)&event, 0)) {
        switch (event.type) {
            case UART_DATA: {
                // Lire les données disponibles
                int len = uart_read_bytes(UART_NUM, data, event.size, 100 / portTICK_PERIOD_MS);
                if (len > 0) {
                    data[len] = '\0'; // Ajouter un terminator de chaîne
                    char *decrypted_data = decode_message(data);
                    if (decrypted_data == NULL) {
                        ESP_LOGE("UART", "Failed to decode the received message");
                        return 0;
                    }

                    strcpy(data, decrypted_data);
                    free(decrypted_data);
                    return strlen(data);
                }{
                    return 0;
                }
                break;
            }
            case UART_FIFO_OVF:
                ESP_LOGW("LoRa", "Overflow du FIFO UART !");
                uart_flush_input(UART_NUM);
                xQueueReset(uart_queue);
                return 0;
                break;
            case UART_BUFFER_FULL:
                ESP_LOGW("LoRa", "Tampon UART plein !");
                uart_flush_input(UART_NUM);
                xQueueReset(uart_queue);
                return 0;
                break;
            default:
                ESP_LOGW("LoRa", "Événement UART non géré : %d", event.type);
                return 0;
                break;
        }
    }
    return 0;
}

// Envoyer un message via LoRa
void envoieLora(const char* msg) {
    char *encrypted = encode_message(msg);
    if (encrypted == NULL) {
        ESP_LOGE("DEBUG", "Failed to encode the message");
        return;
    }
    uart_write_bytes(UART_NUM, encrypted, strlen(encrypted));
    free(encrypted);
    vTaskDelay(1000 / portTICK_PERIOD_MS); // Pause pour ne pas surcharger
}

void send_sensor_data(long long int timestamp, int len) {
    char buffer[128+len];
    len = snprintf(buffer, sizeof(buffer), "timestamp=%lld;%s", timestamp, data);
    // ESP_LOGI("DEBUG", "Sending to uart : %s, %d, fst: %c", buffer, len, buffer[0]);
    uart_write_bytes(UART_NUM_0, buffer, len);
}

void app_main() {
    const int baud_rate = 115200;
    // Initialisation de LoRa
    lora_init();
    // Configuration de l'UART 0 (par défaut)
    uart_config_t uart_config = {
        .baud_rate = baud_rate,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(UART_NUM_0, &uart_config);
    uart_driver_install(UART_NUM_0, UART_BUF_SIZE, 0, 0, NULL, 0);

    bool measuring = false;
    int64_t start_time = 0;

    // Buffer de réception pour les commandes UART
    uint8_t data[UART_BUF_SIZE];
    while (1) {
        // Lire les données depuis l'UART
        int length = uart_read_bytes(UART_NUM_0, data, UART_BUF_SIZE - 1, 20 / portTICK_PERIOD_MS);
        
        if (length > 0) {
            data[length] = '\0'; 

            if (strstr((char*)data, "startMesure") != NULL) {
                envoieLora("Give");
                measuring=true;
                start_time = esp_timer_get_time();
            } else if (strstr((char*)data, "stopMesure") != NULL) {
                envoieLora("Stop");
                measuring = false;
            }
        }

        if (measuring) {
            // Calculer le timestamp (temps écoulé depuis le début des mesures)
            int len = getLora();
            if (len!=0) {
                int64_t current_time = (esp_timer_get_time() - start_time) / 1000000;
                send_sensor_data(current_time, len);
            }
        }
    }
}
